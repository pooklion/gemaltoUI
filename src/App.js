import React, { Component } from 'react';
import axios from 'axios';
import { Modal, Button, Card, Grid, Container } from 'semantic-ui-react';
import Cards from './components/Cards';
import FormNewHero from './components/FormNewHero';

const API_BASE = (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1") ? 'http://localhost:3001' : 'https://gemalto-heros-api.herokuapp.com';
// const API_BASE = 'https://gemalto-heros-api.herokuapp.com'
const endPoint = str => API_BASE + str;

class App extends Component {
  state = {
    heroes: [],
    modalOpen: false
  };

  _loadHeros = () => {
    const self = this;
    axios.get(endPoint('/users/foods')).then(res => {
      const { data } = res;
      self.setState({ heroes: data });
    });
  };

  addNewHero = newHero => {
    const self = this;
    axios.post(endPoint('/users/food'), newHero).then(res => {
      self._loadHeros();
    });
    self.handleClose();
  };

  deleteHero = hero_name => {
    const self = this;
    axios.delete(endPoint(`/users/food/${hero_name}`)).then(res => {
      self._loadHeros();
    });
  }

  componentDidMount = () => this._loadHeros();

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const { heroes, modalOpen } = this.state;
    return (
      <Container>
      <Grid centered padded>
        <Grid.Row>
          <Modal
            size="mini"
            closeIcon
            open={modalOpen}
            onClose={this.handleClose}
            trigger={
              <Button
                onClick={this.handleOpen}
                content="Add New Hero"
                icon="add"
                labelPosition="left"
                color="blue"
              />
            }
          >
            <Modal.Header>Add new hero</Modal.Header>
            <Modal.Content>
              <FormNewHero onSubmit={this.addNewHero} />
            </Modal.Content>
          </Modal>
        </Grid.Row>
        <Grid.Row>
          <Card.Group centered>
            {heroes && heroes.map((hero) => <Cards key={hero.hero_name} hero={hero} onDelete={this.deleteHero}/>)}
          </Card.Group>
        </Grid.Row>
      </Grid>
      </Container>
    );
  }
}

export default App;
