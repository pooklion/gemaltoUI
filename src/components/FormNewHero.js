import React from 'react';
import { Form, Button } from 'semantic-ui-react';

export default class FormNewHero extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hero: {
        first_name: '',
        last_name: '',
        hero_name: '',
        favorite_food: ''
      },
      isSubmitted: false
    };
  }

  // isValid = () => {
  //   const { hero } = this.state;
  //   const err = [];
  //   Object.keys(hero).forEach(key => {
  //     if (!hero[key]) err.push(`${key} is required`);
  //   });
  //   if (err.length) return false;
  //   return true;
  // };

  handleChange = event => {
    let { hero } = this.state;
    hero = {
      ...hero,
      [event.target.name]: event.target.value
    };
    this.setState({
      hero,
      isSubmitted: false
    });
  };

  handleSubmit = event => {
    this.setState({ isSubmitted: true });
    const { onSubmit } = this.props;
    event.preventDefault();
    if (onSubmit) onSubmit(this.state.hero);
  };

  render() {
    const { isSubmitted, hero } = this.state;
    const inputList = [
      { field: 'first_name', label: 'First Name'},
      { field: 'last_name', label: 'Last Name'},
      { field: 'hero_name', label: 'Hero Name'}
    ]

    return (
      <Form onSubmit={this.handleSubmit}>
      {inputList.map(inp => (
        <Form.Input 
          key={inp.field} 
          error={isSubmitted && !hero[inp.field]}
          label={inp.label}
          type="text"
          name={inp.field}
          value={hero[inp.field]}
          onChange={this.handleChange}
          required />
        ))}
        <Form.Input 
          key='favorite_food'
          label='Favorite Food'
          type='text'
          name='favorite_food'
          value={hero.favorite_food}
          onChange={this.handleChange}/>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}
