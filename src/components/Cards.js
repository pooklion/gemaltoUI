import React from 'react';
import { Card, Button } from 'semantic-ui-react';

export default function Cards({ hero, onDelete }) {
  return (
    <Card className='bg' color="blue" style={{textTransform: 'capitalize'}}>
      <Card.Content className='cardHeader' header={hero.hero_name} />
      <Card.Content>
        <Card.Description>First Name: &nbsp;{hero.first_name}</Card.Description>
        <Card.Description>Last Name: &nbsp;{hero.last_name}</Card.Description>
        <Card.Description>
          Favorite Food: &nbsp;
          <span style={{fontWeight: 'bold'}}>
          {hero.favorite_foods && hero.favorite_foods.map(i=>i.food).filter(f=>f!=='').join(',')}
          </span>
        </Card.Description>
      </Card.Content>
      <Card.Content className='cardBtn'>
        <Button inverted color='orange' onClick={() => { onDelete(hero.hero_name) }}>Delete</Button>
      </Card.Content>
    </Card>
  );
}
